#include "Fahrenheit.hpp"
#include <sstream>

using namespace std;

Fahrenheit :: Fahrenheit(){

}

Fahrenheit :: ~Fahrenheit(){


}
/*void Fahrenheit :: setTemp(){
	float Temperatura_Entrada;
	cin >> Temperatura_Entrada;
	temp = Temperatura_Entrada;
}

float Fahrenheit :: getTemp(){
	return temp;
}*/
 
string Fahrenheit :: conversaoTemperatura(float temp){
        float celsius=0.0, kelvin=0.0;
	
	string resultado;
        string KelvinResultado;
	string CelsiusResultado; 

        celsius =  (5*(temp-32)/9);
	kelvin = celsius + 273;

	ostringstream convert_K;
	convert_K << kelvin;
	KelvinResultado = convert_K.str();	

	ostringstream convert_C;
	convert_C << celsius;
	CelsiusResultado = convert_C.str();		

        resultado = "Kelvin: " + KelvinResultado + "\n" + "Celsius: " + CelsiusResultado + "\n";
        
return resultado;
}

