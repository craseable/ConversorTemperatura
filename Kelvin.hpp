#ifndef KELVIN_HPP
#define KELVIN_HPP

#include "MedidorTemperatura.hpp"

using namespace std;

class Kelvin : public MedidorTemperatura{
public:
        Kelvin();
        ~Kelvin();
        
	
	void setTemp();
	float getTemp();

     	string conversaoTemperatura(float temp);

};

#endif

