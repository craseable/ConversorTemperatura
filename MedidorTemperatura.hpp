#ifndef MEDIDORTEMPERATURA_HPP
#define MEDIDORTEMPERATURA_HPP

#include <string>
#include <iostream>

using namespace std;

class MedidorTemperatura{
private:
	float temp;
public:
	
	void setTemp();
	float getTemp();

	string conversaoTemperatura();
	
};
#endif
