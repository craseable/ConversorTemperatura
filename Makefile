all: celsius.o Fahrenheit.o Kelvin.o main.o MedidorTemperatura.o
	g++ -o main celsius.o Fahrenheit.o Kelvin.o main.o MedidorTemperatura.o
MedidorTemperatura.o: MedidorTemperatura.cpp MedidorTemperatura.hpp
	g++ -c MedidorTemperatura.cpp

celsius.o: celsius.cpp celsius.hpp
	g++ -c celsius.cpp

Fahrenheit.o: Fahrenheit.cpp Fahrenheit.hpp
	g++ -c Fahrenheit.cpp

Kelvin.o: Kelvin.cpp Kelvin.hpp
	g++ -c Kelvin.cpp

main.o: 
	g++ -c main.cpp

clean:
	rm -rf *.o

run:
	./main
	 

