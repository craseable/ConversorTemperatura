Programa que faz conversão de temperatura com três unidades de medida.
(1) Celsius
(2) Fahrenheit
(3) Kelvin

Utiliza conceitos de OO, como herança, classes, polimorfismo.

Para compilar o programa, digite 

make

Para executá-lo, digite:

make run

Para limpar todos os objetos:

make clean
