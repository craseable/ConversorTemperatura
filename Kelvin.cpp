#include "Kelvin.hpp"
#include <sstream>

using namespace std;

Kelvin :: Kelvin(){


}
Kelvin :: ~Kelvin(){


}

/*void Kelvin :: setTemp(){
	float Temperatura_Entrada;
	cin >> Temperatura_Entrada;
	temp = Temperatura_Entrada;
}

float Kelvin :: getTemp(){
	return temp;
}*/
 
string Kelvin :: conversaoTemperatura(float temp){
        float fahrenheit=0.0, celsius=0.0;
        string resultado;
	
	string CelsiusResultado;
	string FahrenheitResultado;
	
        celsius = temp - 273;  
        fahrenheit =  (1.8*(celsius)) + 32;
	
	ostringstream convert_C;
	convert_C << celsius;
	CelsiusResultado = convert_C.str();

	ostringstream convert_F;
	convert_F << fahrenheit;
	FahrenheitResultado = convert_F.str();

        resultado = "Celsius: " + CelsiusResultado + "\n" + "Fahrenheit: " + FahrenheitResultado + "\n";
        
return resultado;
}

