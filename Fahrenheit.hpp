#ifndef FAHRENHEIT_HPP
#define FAHRENHEIT_HPP

#include "MedidorTemperatura.hpp"

using namespace std;

class Fahrenheit : public MedidorTemperatura{

public:
	Fahrenheit();
	~Fahrenheit();
		
	
	void setTemp();
	float getTemp();

	string conversaoTemperatura(float temp);


};

#endif
