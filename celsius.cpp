#include "celsius.hpp"
#include <sstream>

using namespace std;

Celsius :: Celsius(){


}
Celsius :: ~Celsius(){


}
/*void Celsius :: setTemp(){
	float Temperatura_Entrada;
	cin >> Temperatura_Entrada;
	temp = Temperatura_Entrada;
}

float Celsius :: getTemp(){
	return temp;
}*/

string Celsius :: conversaoTemperatura(float temp){
	float fahrenheit=0.0, kelvin=0.0;

	string resultado;
	string FahrenheitResultado;
	string KelvinResultado;

	kelvin = 273 + temp;
	fahrenheit =  (9*temp/5) + 32;

	ostringstream convert_K;
	convert_K << kelvin;
	KelvinResultado = convert_K.str();	

	ostringstream convert_F;
	convert_F << fahrenheit;
	FahrenheitResultado = convert_F.str();	

	resultado = "Kelvin: " + KelvinResultado + "\n" + "Fahrenheit: " + FahrenheitResultado + "\n";
	
return resultado;
}
