#ifndef CELSIUS_HPP
#define CELSIUS_HPP

#include "MedidorTemperatura.hpp"

using namespace std;

class Celsius : public MedidorTemperatura{

public:
	Celsius();
	~Celsius();

	void setTemp();
	float getTemp();
	
	string conversaoTemperatura(float temp);

};

#endif
